// eslint-disable-next-line no-undef
module.exports = {
    "env": {
        "browser": false,
        "es2021": true,
        "cypress/globals": true,
    },
    "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:cypress/recommended",
    ],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": "latest"
    },
    "plugins": [
        "@typescript-eslint",
        "cypress"
    ],
    "rules": {
    }
}
