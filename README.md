# ETX Content Publisher

> Publish content anywhere

## Objectives

- Publish new content to a fully-authenticated REST API 
- Update RSS feeds for each listening platform

## Installation

Requirements: Node >= 14

```bash
$ yarn
```

## Execution

```bash
$ yarn start
```

## Test
```bash
$ yarn test
```