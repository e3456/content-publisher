import { rest } from "msw";
import { facebookMock } from "./providers/facebook";
import { linkedinMock } from "./providers/linkedin";
import { spotifyMock } from "./providers/spotify";
import { twitterMock } from "./providers/twitter";
import { deezerMock } from "./providers/deezer";
import { tiktokMock } from "./providers/tiktok";
import { youtubeMock } from "./providers/youtube";

const twitterHandlers = [
  rest.post(twitterMock.authorizationURI, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        access_token: "twitter-access_token",
        refresh_token: "twitter-refresh_token",
      })
    );
  }),
  rest.post(twitterMock.publishURI, (_, res, ctx) => {
    return res(
      ctx.status(201),
      ctx.json({
        data: {
          id: "twitter-id-new-content",
        },
      })
    );
  }),
  rest.post(twitterMock.uploadURI, (_, res, ctx) => {
    return res(
      ctx.status(201),
      ctx.json({
        media_id_string: "twitter-id-new-media",
      })
    );
  }),
];

const spotifyHandlers = [
  rest.post(spotifyMock.authorizationURI, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        access_token: "spotify-access_token",
        token_type: "Bearer",
        scope: "user-read-private user-read-email",
        expires_in: 3600,
        refresh_token: "spotify-refresh_token",
      })
    );
  }),
];

const linkedinHandlers = [
  rest.post(linkedinMock.authorizationURI, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        access_token: "linkedin-access_token",
        expires_in: 3600,
      })
    );
  }),
  rest.post(linkedinMock.publishURI, (_, res, ctx) => {
    return res(
      ctx.status(201),
      ctx.json({
        activity: "urn:li:activity:6275832358151294976",
        content: {
          contentEntities: [
            {
              entityLocation: "https://www.example.com/content.html",
              thumbnails: [
                {
                  authors: [],
                  imageSpecificContent: {},
                  publishers: [],
                  resolvedUrl: "https://www.example.com/image.jpg",
                },
              ],
            },
          ],
          title: "Test Share with Content",
        },
        created: {
          actor: "urn:li:person:324_kGGaLE",
          time: 1496275033520,
        },
        distribution: {
          linkedInDistributionTarget: {
            visibleToGuest: false,
          },
        },
        edited: false,
        id: "linkedin-id-new-content",
        lastModified: {
          actor: "urn:li:person:324_kGGaLE",
          time: 1496275033520,
        },
        owner: "urn:li:organization:123456789",
        subject: "Test Share Subject",
        text: {
          text: "Test Share!",
        },
      })
    );
  }),
  rest.post(linkedinMock.uploadURI, (_, res, ctx) =>
    res(
      ctx.status(201),
      ctx.json({
        value: {
          uploadMechanism: {
            "com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest": {
              headers: {
                "media-type-family": "STILLIMAGE",
              },
              uploadUrl:
                "https://api.linkedin.com/mediaUpload/C5522AQHn46pwH96hxQ/feedshare-uploadedImage/0?ca=vector_feedshare&cn=uploads&m=AQLKRJOn_yNw6wAAAW2T0DWnRStny4dzsNVJjlF3aN4-H3ZR9Div77kKoQ&app=1983914&sync=0&v=beta&ut=1Dnjy796bpjEY1",
            },
          },
          mediaArtifact:
            "urn:li:digitalmediaMediaArtifact:(urn:li:digitalmediaAsset:C5522AQHn46pwH96hxQ,urn:li:digitalmediaMediaArtifactClass:feedshare-uploadedImage)",
          asset: "urn:li:digitalmediaAsset:C5522AQHn46pwH96hxQ",
        },
      })
    )
  ),
];

const facebookHandlers = [
  rest.get(facebookMock.authorizationURI, (_, res, ctx) =>
    res(
      ctx.status(200),
      ctx.json({
        access_token: "facebook-access_token",
        expires_in: 3600,
        type: "bearer",
      })
    )
  ),
  rest.get("https://graph.facebook.com/v13.0/oauth/client_code", (_, res, ctx) =>
    res(
      ctx.status(200),
      ctx.json({
        access_token: "facebook-access_token",
        expires_in: 360000,
        type: "bearer",
      })
    )
  ),
  rest.all(facebookMock.publishURI + "*", (_, res, ctx) =>
    res(
      ctx.status(201),
      ctx.json({
        id: "facebook-id-new-content",
      })
    )
  ),
];

const tiktokHandlers = [
  rest.post(tiktokMock.authorizationURI, (_, res, ctx) =>
    res(
      ctx.status(200),
      ctx.json({
        open_id: "tiktok-user_id",
        scope: tiktokMock.scope.join(","),
        access_token: "tiktok-access_token",
        expires_in: 3600,
        refresh_token: "tiktok-refresh_token",
        refresh_expires_in: 360000,
      })
    )
  ),
  rest.post(tiktokMock.publishURI, (_, res, ctx) =>
    res(
      ctx.status(201),
      ctx.json({
        share_id: "tiktok-id-new-audio",
      })
    )
  ),
  rest.post(tiktokMock.uploadURI, (_, res, ctx) =>
    res(
      ctx.status(201),
      ctx.json({
        share_id: "tiktok-id-new-video",
      })
    )
  ),
];

const deezerHandlers = [
  rest.get(deezerMock.authorizationURI, (_, res, ctx) =>
    res(
      ctx.status(200),
      ctx.json({
        access_token: "deezer-access_token",
        expires: 3600,
      })
    )
  ),
];

const youtubeHandlers = [
  rest.post(youtubeMock.authorizationURI, (_, res, ctx) =>
    res(
      ctx.status(200),
      ctx.json({
        access_token: "youtube-access_token",
        expires_in: 3920,
        token_type: "Bearer",
        scope: "https://www.googleapis.com/auth/youtube.force-ssl",
        refresh_token: "youtube-refresh_token",
      })
    )
  ),
  rest.post(youtubeMock.publishURI, (_, res, ctx) =>
    res(
      ctx.status(201),
      ctx.json({
        id: "youtube-id-new-content",
      })
    )
  ),
];

const itunesHandler = [
  rest.get("https://itunes.apple.com/search", (_, res, ctx) =>
    res(ctx.status(200), ctx.json({ trackId: "itunes-id-content" }))
  ),
];

const pubsubHandler = [rest.post("https://pubsubhubbub.appspot.com", (_, res, ctx) => res(ctx.status(204)))];

const localHandlers = [
  rest.get("http://localhost:5000/files/audio.mp3", (_, res, ctx) => res(ctx.status(200), ctx.text("123456789"))),
  rest.all("http://localhost:5000/*", (_, res, ctx) => res(ctx.status(200))),
];

export const handlers = [
  ...facebookHandlers,
  ...linkedinHandlers,
  ...twitterHandlers,
  ...tiktokHandlers,
  ...youtubeHandlers,
  ...spotifyHandlers,
  ...deezerHandlers,
  ...itunesHandler,
  ...localHandlers,
  ...pubsubHandler,
];
