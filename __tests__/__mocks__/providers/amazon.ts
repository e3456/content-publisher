export const amazonMock = {
  id: "11",
  name: "amazon",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rss",
  clientId: "",
  clientSecret: "",
  publicURL: "",
  registerURI: "",
  authorizationURI: "",
  publishURI: "",
  uploadURI: "",
  scope: [""]
} as const;
