export const deezerMock = {
  id: "6",
  name: "deezer",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rss",
  clientId: "",
  clientSecret: "",
  publicURL: "https://podcasters.deezer.com/submission",
  registerURI: "https://connect.deezer.com/oauth/auth.php",
  authorizationURI: "https://connect.deezer.com/oauth/access_token.php",
  publishURI: "https://graph.deezer.com/",
  uploadURI: "",
  scope: ["basic_access", "email"]
} as const;
