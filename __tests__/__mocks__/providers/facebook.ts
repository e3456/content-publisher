export const facebookMock = {
  id: "2",
  name: "facebook",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rest",
  clientId: "",
  clientSecret: "",
  registerURI: "https://www.facebook.com/v13.0/dialog/oauth/",
  authorizationURI: "https://graph.facebook.com/oauth/access_token/",
  publishURI: "https://graph.facebook.com/",
  uploadURI: "",
  scope: ["email", "pages_manage_posts", "pages_read_engagements"]
} as const;
