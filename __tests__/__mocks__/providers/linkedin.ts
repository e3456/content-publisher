export const linkedinMock = {
  id: "3",
  name: "linkedin",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rest",
  clientId: "",
  clientSecret: "",
  registerURI: "https://www.linkedin.com/oauth/v2/authorization",
  authorizationURI: "https://www.linkedin.com/oauth/v2/accessToken",
  publishURI: "https://api.linkedin.com/v2/shares",
  uploadURI: "https://api.linkedin.com/v2/assets",
  scope: ["r_liteprofile", "r_emailaddress", "w_member_social"]
} as const;
