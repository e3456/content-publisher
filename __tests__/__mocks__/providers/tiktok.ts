export const tiktokMock = {
  id: "7",
  name: "tiktok",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rest",
  clientId: "",
  clientSecret: "",
  registerURI: "https://open-api.tiktok.com/platform/oauth/connect/",
  authorizationURI: "https://open-api.tiktok.com/oauth/access_token/",
  publishURI: "https://open-api.tiktok.com/share/sound/upload/",
  uploadURI: "https://open-api.tiktok.com/share/video/upload/",
  scope: ["user.info.basic", "video.list", "video.upload", "sound.share.create"]
} as const;
