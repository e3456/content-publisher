export const twitterMock = {
  id: "5",
  name: "twitter",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rest",
  clientId: "",
  clientSecret: "",
  registerURI: "https://twitter.com/i/oauth2/authorize",
  authorizationURI: "https://api.twitter.com/2/oauth2/token",
  publishURI: "https://api.twitter.com/2/tweets",
  uploadURI: "https://upload.twitter.com/1.1/media/upload.json",
  scope: ["tweet.read", "tweet.write", "offline.access"]
} as const;
