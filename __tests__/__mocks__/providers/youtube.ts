export const youtubeMock = {
  id: "8",
  name: "youtube",
  createdAt: new Date(2022, 1, 25, 14),
  updatedAt: new Date(2022, 1, 25, 14),
  type: "rest",
  clientId: "",
  clientSecret: "",
  registerURI: "https://accounts.google.com/o/oauth2/v2/auth",
  authorizationURI: "https://oauth2.googleapis.com/token",
  publishURI: "https://youtube.googleapis.com/youtube/v3/videos",
  uploadURI: "",
  scope: ["https://www.googleapis.com/auth/youtube.force-ssl", "https://www.googleapis.com/auth/youtube.upload"]
} as const;
