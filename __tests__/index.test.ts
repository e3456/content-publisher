import got from "got";
import * as models from "./__mocks__/providers";
import * as distributors from "../src/providers";
import * as S3 from "../src/lib/rss";
import { Distributor as IDistributor } from "../src/typings";
import { Distributor as CDistributor } from "../src/providers/distributor";

import { initProvider, listProviders } from "../src/index";

const itif = (condition: boolean) => (condition ? it : it.skip);
const describeif = (condition: boolean) => (condition ? describe : describe.skip);
const describetodo =
  (condition: boolean) =>
  (...args: Parameters<typeof describe>) =>
    condition ? describe(...args) : it.todo(args[0] as string);

describe("Distributors", () => {
  it("list all available distributors", async () => {
    const providers = await listProviders("userId");
    expect(Array.isArray(providers)).toBe(true);
    expect(providers.length).toEqual(Object.keys(distributors).length);
    expect(typeof providers[0].selectable).toBe("boolean");
    expect(typeof providers[0].hasPages).toBe("boolean");
    expect(typeof providers[0].name === "string" && providers[0].name).toBeTruthy();
    expect(providers[0].type === "rest" || providers[0].type === "rss").toBe(true);
  });

  Object.entries(models).forEach(([mockName, model]) => {
    const name = mockName.slice(0, -4);

    describetodo(name in distributors)(name, () => {
      let instance: IDistributor & InstanceType<typeof CDistributor>;

      beforeEach(() => {
        instance = initProvider(name as keyof typeof distributors, "userId");
      });

      describe("register", () => {
        it("exists", () => {
          expect(typeof instance.register).toBe("function");
        });
        it("yields a string", async () => {
          const result = await instance.register();
          expect(typeof result).toBe("string");
        });
        it("yields a url", async () => {
          const result = await instance.register();
          expect(new URL(result)).toBeTruthy();
        });
        it("yields a url with the right parameters", async () => {
          const result = new URL(await instance.register());
          expect(result.search).toMatch(instance.scope[0]);
          expect(result.search).toMatch(instance.clientId);
        });
      });

      describeif(!!model.authorizationURI)("authorize", () => {
        let spySet: ReturnType<typeof jest.spyOn>;

        beforeEach(() => {
          spySet = jest.spyOn(instance, "setUserCredentials");
        });

        afterEach(() => {
          spySet.mockReset();
        });

        it("exists", () => {
          expect(typeof instance.authorize).toBe("function");
        });
        it("yields", (done) => {
          instance
            .authorize("code", "userId")
            .then(() => expect(true).toBe(true))
            .catch((err) => expect(err).not.toBeDefined())
            .finally(done);
        });
        it("stores the credentials", async () => {
          await instance.authorize("code", "userId");
          expect(spySet).toHaveBeenCalledTimes(1);
          expect(spySet.mock.calls[0][0]).toHaveProperty("accessToken");
          expect(spySet.mock.calls[0][0].accessToken).toMatch("access_token");
        });
      });

      describeif(!!model.authorizationURI)("authenticate", () => {
        it("exists", () => {
          expect(typeof instance.authenticate).toBe("function");
        });

        it("yields a tuple", async () => {
          const url = "http://localhost:5000/path/to/endpoint";
          const body = JSON.stringify({ id: "id" });
          const req = new URL(url);
          const result = await instance.authenticate(req, { body });
          expect(typeof result).toBe("object");
          expect(result[0].toString()).toMatch(url);
          expect(typeof result[1]).toBe("object");
          expect(result[1].body).toEqual(body);
        });

        it("injects the access token somewhere", async () => {
          const [url, options = {}] = await instance.authenticate("http://localhost:5000/path/to/endpoint");
          const { headers = {} } = options;
          const match = Object.values(headers).find(
            (header) => typeof header === "string" && header.match("-access_token")
          );
          if (match) expect(match).toBeDefined();
          else expect(url.toString()).toMatch(/.+\?*-access_token/);
        });
      });

      describe("publish", () => {
        let content;

        beforeEach(() => {
          content = {
            audio: "http://localhost:5000/files/audio.mp3",
            text: "My content audio",
            pageId: "pageId",
          };
        });

        it("exists", () => {
          expect(typeof instance.publish).toBe("function");
        });

        it("yields a string", async () => {
          const result = await instance.publish(content);
          expect(typeof result).toBe("string");
        });

        describeif(model.type === "rss")("rss", () => {
          let spyRSS: ReturnType<typeof jest.spyOn>;

          beforeEach(() => {
            spyRSS = jest.spyOn(S3, "sendRSSFeed");
          });
          afterEach(() => {
            spyRSS.mockReset();
            spyRSS.mockRestore();
          });

          it("yields a XML file", async () => {
            await instance.publish(content);
            const result = spyRSS.mock.calls[0][1];
            expect(result).toMatch('<?xml version="1.0"?>');
            expect(result).toMatch(
              '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:content="http://purl.org/rss/1.0/modules/content/">'
            );
          });

          it("prints the content URL and title", async () => {
            await instance.publish(content);
            const result = spyRSS.mock.calls[0][1];
            expect(result).toMatch(content.audio);
            expect(result).toMatch(content.text);
          });

          itif(!!model.publishURI)("tries to refresh the feed", async () => {
            const spyGot = jest.spyOn(got, "post");
            await instance.publish(content);
            const result = spyGot.mock.calls[0][0];
            expect(result).toEqual("https://pubsubhubbub.appspot.com/");
            spyGot.mockRestore();
          });
        });

        describeif(model.type === "rest")("rest", () => {
          let spyGot: ReturnType<typeof jest.spyOn>;
          beforeEach(() => {
            spyGot = jest.spyOn(got, model.name === "facebook" ? "get" : "post");
          });
          afterEach(() => {
            spyGot.mockReset();
            spyGot.mockRestore();
          });

          it("yields a string with the correct ID", async () => {
            const result = await instance.publish(content);
            expect(result).toBe(`${instance.name}-id-new-content`);
          });

          it("sends a request containing the content text", async () => {
            await instance.publish(content);
            const calls = spyGot.mock.calls;
            const lastCall = calls[calls.length - 1];
            const url = lastCall[0].toString();
            try {
              expect(url).toMatch(content.text.split(" ").join("+"));
            } catch (err) {
              const opts = lastCall[1];
              if (!opts) expect(false).toBe(true);
              if (opts.json) expect(JSON.stringify(opts.json)).toMatch(content.text);
              else expect(Object.values(opts).some((opt) => JSON.stringify(opt).match(content.text))).toBe(true);
            }
          });
        });
      });
    });
  });
});
