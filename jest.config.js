/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
// eslint-disable-next-line no-undef
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFilesAfterEnv: ["<rootDir>/__tests__/setupTests.ts"],
  testMatch: ["**/__tests__/**/*.test.ts"],
  watchPathIgnorePatterns: [
    "<rootDir>/src/lib/.+\\.client_secret\\.json$",
    "<rootDir>/src/.+\\.xml$",
  ],
  globals: {
    'ts-jest': {
      tsconfig: "tsconfig.test.json"
    }
  }
};