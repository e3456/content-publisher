const getFakeUser = (id = "1") => ({
  email: "superuser@email.com",
  name: "Super User",
  website: "https://superuser.website.com",
  imageLink: "https://superuser.website.com/assets/image.png",
  language: "fr",
  id,
});

export const getUser = async (userId: string) => {
  const user = await getFakeUser(userId);
  return user;
}