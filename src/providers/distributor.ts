import { Credentials, IDistributor, Distributor as CDistributor } from "../typings";
import { getSecret, setSecret } from "../lib/secret";

const OAUTH_SERVER_URI = process.env.VERCEL_URL ? `https://${process.env.VERCEL_URL}` : 'http://localhost:5000';

export class Distributor implements IDistributor {
  name: string;
  type: "rss" | "rest";
  description?: string;
  publicURL?: string;
  clientId: string;
  scope: string[];
  registerURI: string;
  authorizationURI: string;
  publishURI: string;
  uploadURI: string;
  state: string;

  private static appCredentials: Map<string, { clientId: string; clientSecret: string }> = new Map();
  private static userCredentials: Map<string, Credentials> = new Map();

  constructor(state: string) {
    this.state = state;
  }

  get redirectURI(): string {
    const redirectURI = new URL(`/providers/${this.name}/authorize`, OAUTH_SERVER_URI);
    return redirectURI.toString();
  }

  get rssFeedURI(): string {
    const uri = new URL(`/providers/${this.name}/feeds/${this.state}`, "http://localhost:5000");
    return uri.toString();
  }

  get json() {
    return {
      name: this.name,
      type: this.type,
      description: this.description,
      clientId: this.clientId,
      publicURL: this.publicURL,
      scope: this.scope,
      registerURI: this.registerURI,
    } as const;
  }

  get secretName() {
    return `${this.state}.${this.name}`;
  }
  get isRegistered() {
    const hasClientId = Boolean(this.clientId && typeof this.clientId === "string");
    if (process.env.NODE_ENV === "production") return hasClientId && this.clientId.startsWith("test.");
    return hasClientId;
  }

  async getAppCredentials(): Promise<{ clientId: string; clientSecret: string }> {
    if (Distributor.appCredentials.has(this.name)) return Distributor.appCredentials.get(this.name);

    let clientSecret = await getSecret(`${this.name}.client_secret`);
    if (typeof clientSecret === "object" && clientSecret.accessToken) {
      clientSecret = clientSecret.accessToken as string;
    }
    if (!clientSecret || typeof clientSecret !== "string")
      throw new Error(`No client secret found for provider ${this.name}`);
    const creds = { clientId: this.clientId, clientSecret };
    Distributor.appCredentials.set(this.name, creds);
    return creds;
  }

  async getUserCredentials(userId = this.state, callback?: CDistributor["refresh"]): Promise<Credentials> {
    if (!this.name || !userId) return {} as Credentials;

    let creds = Distributor.userCredentials.get(this.secretName);
    try {
      if (!creds) creds = await getSecret(this.secretName);

      if (
        typeof callback === "function" &&
        creds.expiresAt <= new Date().getTime() &&
        (creds.refreshToken || creds.accessToken)
      ) {
        const newCreds = await callback(creds.refreshToken ?? creds.accessToken, userId);
        if (newCreds && typeof newCreds === "object") {
          Object.assign(creds, newCreds);
          await this.setUserCredentials(creds);
        }
      }
    } catch (e) {
      // nothing to do
    }

    return creds;
  }

  async setUserCredentials(creds: Credentials): Promise<void> {
    if (!creds) return;
    Distributor.userCredentials.set(this.secretName, creds);
    await setSecret(this.secretName, creds);
  }
}
