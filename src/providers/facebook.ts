import got, { OptionsOfJSONResponseBody } from "got";
import { Credentials, ETXStudioContent, RESTPageDistributor } from "../typings";
import { Distributor } from "./distributor";

type FacebookCredentials = {
  access_token: string;
  expires_in: number;
  type: string;
};

export class Facebook extends Distributor implements RESTPageDistributor {
  name = "facebook";
  type = "rest" as const;
  registerURI = "https://www.facebook.com/v13.0/dialog/oauth";
  authorizationURI = "https://graph.facebook.com/oauth/access_token";
  publishURI = "https://graph.facebook.com/";
  uploadURI = "";
  scope = ["email", "pages_manage_posts", "pages_show_list"];
  clientId = "940151223321752";

  upload?(file: ArrayBuffer): Promise<string> {
    throw new Error("Method not implemented.");
  }

  async register() {
    const loginPage = new URL(this.registerURI);
    loginPage.searchParams.set("redirect_uri", this.redirectURI);
    loginPage.searchParams.set("client_id", this.clientId);
    loginPage.searchParams.set("state", this.state);
    loginPage.searchParams.set("response_type", "code");
    loginPage.searchParams.set("scope", this.scope.join(","));
    return loginPage.toString();
  }

  async authorize(code: string, userId: string = this.state): Promise<void> {
    const authURL = new URL(this.authorizationURI);
    const { clientId, clientSecret } = await this.getAppCredentials();
    authURL.searchParams.set("client_id", clientId);
    authURL.searchParams.set("client_secret", clientSecret);
    authURL.searchParams.set("redirect_uri", this.redirectURI);
    authURL.searchParams.set("code", code);
    let fbCredentials = await got.get(authURL.toString()).json<FacebookCredentials>();

    if (fbCredentials.expires_in <= 3600) {
      authURL.searchParams.delete("code");
      authURL.searchParams.delete("redirect_uri");
      authURL.searchParams.set("grant_type", "fb_exchange_token");
      authURL.searchParams.set("fb_exchange_token", fbCredentials.access_token);
      fbCredentials = await got.get(authURL.toString()).json<FacebookCredentials>();
    }

    const credentials = await this.refresh(fbCredentials.access_token, userId);
    await this.setUserCredentials(credentials);
  }

  async publish(content: ETXStudioContent): Promise<string> {
    const { text, pageId } = content;
    if (!pageId) throw new Error("No page ID");
    const accessToken = await this.getPageAccessToken(pageId);
    const publishURI = new URL(`/v13.0/${pageId}/feed`, this.publishURI);
    publishURI.searchParams.set("message", text);
    publishURI.searchParams.set("access_token", accessToken);
    const publication = await got.get(publishURI.toString()).json<{ id: string }>();
    return publication.id;
  }

  async authenticate(
    url: string | URL,
    options?: OptionsOfJSONResponseBody
  ): Promise<[URL, OptionsOfJSONResponseBody]> {
    if (typeof url === "string") url = new URL(url);
    const creds = await this.getUserCredentials(this.state, this.refresh.bind(this));
    url.searchParams.set("access_token", creds.accessToken);
    return [url, options];
  }

  async refresh(token: string, userId = this.state): Promise<Credentials> {
    const refreshURI = new URL("/v13.0/oauth/client_code", "https://graph.facebook.com");
    const { clientId, clientSecret } = await this.getAppCredentials();
    refreshURI.searchParams.set("client_id", clientId);
    refreshURI.searchParams.set("client_secret", clientSecret);
    refreshURI.searchParams.set("redirect_uri", this.redirectURI);
    refreshURI.searchParams.set("access_token", token);

    const fbCredentials = await got.get(refreshURI.toString()).json<FacebookCredentials>();

    return {
      distributor: this.name,
      userId,
      accessToken: fbCredentials.access_token,
      expiresAt: new Date().getTime() + fbCredentials.expires_in * 1000,
    };
  }

  async getPagesList(): Promise<{ id: string; name: string }[]> {
    const fbUserId = await this.getMetaUserId();
    if (!fbUserId) return [];
    const pageURL = new URL(`/v13.0/${fbUserId}/accounts`, "https://graph.facebook.com");
    pageURL.searchParams.set("fields", "name,id");
    const [url] = await this.authenticate(pageURL);
    const { data: pages = [] } = await got.get(url).json<{ data: { id: number; name: string }[] }>();
    return pages.map(({ id, ...page }) => ({ id: id.toString(), ...page }));
  }

  protected async getMetaUserId(): Promise<string> {
    const meURL = new URL("/v13.0/me", "https://graph.facebook.com");
    meURL.searchParams.set("fields", "id");
    const [url] = await this.authenticate(meURL);
    const { id: fbUserId } = await got.get(url).json<{ id: number }>();
    return fbUserId.toString();
  }

  protected async getPageAccessToken(pageId: string): Promise<string> {
    const pageURL = new URL(`/v13.0/${pageId}`, "https://graph.facebook.com");
    pageURL.searchParams.set("fields", "access_token");
    const [url] = await this.authenticate(pageURL);
    return got
      .get(url.toString())
      .json<FacebookCredentials>()
      .then((res) => res.access_token);
  }
}
