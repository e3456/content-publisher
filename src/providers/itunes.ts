import got, { OptionsOfJSONResponseBody } from "got";
import { Distributor } from "./distributor";
import { Credentials, ETXStudioContent, RSSDistributor } from "../typings";
import { createRSSFeed, mergeItemWithFeed, sendRSSFeed } from "../lib/rss";
import { getUser } from "../lib/user";
import { basename } from "path";

export class ITunes extends Distributor implements RSSDistributor {
  name = "itunes";
  type = "rss" as const;
  publicURL = "https://podcastsconnect.apple.com/login?targetUrl=%2F";
  registerURI = "";
  authorizationURI = "";
  publishURI = "";
  uploadURI = "";
  scope = [""];
  clientId = "test.itunes.client_id";

  async check(content: ETXStudioContent): Promise<boolean> {
    return true;
  }
  async register(): Promise<string> {
    const { clientId } = await this.getAppCredentials();
    const loginPage = new URL(this.publicURL);
    loginPage.searchParams.set("client_id", clientId);
    return loginPage.toString();
  }
  async authorize(code: string, userId: string = this.state): Promise<void> {
    return;
  }
  async authenticate(
    url: string | URL,
    options?: OptionsOfJSONResponseBody
  ): Promise<[URL, OptionsOfJSONResponseBody]> {
    return [new URL(url), options];
  }
  async refresh(token: string, userId = this.state): Promise<Credentials> {
    return { userId, accessToken: token, distributor: this.name, expiresAt: Infinity };
  }

  async publish(content: ETXStudioContent): Promise<string> {
    const user = await getUser(this.state);
    let feed: string;
    try {
      feed = await got.get(this.rssFeedURI).text();
    } catch (e) {
      // nothing to do
    }
    if (!feed) {
      feed = createRSSFeed(
        {
          title: `Podcast - ${user.name}`,
          description: `Émission de ${user.name}`,
          language: user.language ?? "fr",
          link: user.website,
          image: user.imageLink,
          category: [],
          author: user.name,
          owner: user.email,
        },
        { pubsub: this.publishURI }
      );
    }
    const guid = `${content.audio}-${this.state}`;
    const body = mergeItemWithFeed(
      {
        title: content.text,
        description: content.text,
        author: user.name,
        category: [],
        content: {
          size: 1000,
          type: "audio/mp3",
          url: content.audio,
        },
        guid,
        link: user.website,
      },
      feed
    );
    const name = `${this.state}/${basename(content.audio)}`
    const result = await sendRSSFeed(name, body);
    if (this.publishURI) await this.check(content);
    return result;
  }
}
