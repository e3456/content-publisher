import express from "express";
import cors from "cors";
import { initProvider, listProviders } from ".";
import { ETXStudioContent } from "./typings";

const app = express();
app.use(express.json());
app.use(cors());

const router = express.Router();
router
  .use((req, res, next) => {
    res.locals.userId = (req.query.userId as string) || "userId";
    next();
  })
  .get("/:name/register", async (req, res, next) => {
    try {
      const { name } = req.params;
      const provider = initProvider(name, res.locals.userId);
      const redirectURL = await provider.register();
      return res.redirect(redirectURL);
    } catch (err) {
      next(err);
    }
  })
  .get("/:name/authorize", async (req, res, next) => {
    try {
      const { name } = req.params;
      const { code, state } = req.query;
      const provider = initProvider(name, state as string);
      await provider.authorize(code as string);
      return res.status(202).send();
    } catch (err) {
      next(err);
    }
  })
  .post("/:name/publish", async (req, res, next) => {
    try {
      const content = req.body as ETXStudioContent;
      const { name } = req.params;
      const provider = initProvider(name, res.locals.userId);
      const result = await provider.publish(content);
      return res.status(201).json(result);
    } catch (err) {
      next(err);
    }
  })
  .get("/:name/feeds/:userId", async (req, res, next) => {
    try {
      res.sendFile(`/home/pascal/workspace/etx/etx-content-publisher/src/${req.params.userId}.xml`)
    } catch (err) {
      next(err);
    }
  })
  .get("/", async (_, res, next) => {
    try {
      const data = await listProviders(res.locals.userId);
      return res.json({ data });
    } catch (err) {
      next(err);
    }
  });

app.use("/providers", router);
app.get("/feeds/:name", (req, res) => res.sendFile(`/home/pascal/workspace/etx/etx-content-publisher/src/${req.params.name}.xml`));
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err, _, res, _next) => {
  if (process.env.NODE_ENV !== "production") console.error(err.stack);
  return res.status(err.statusCode ?? 500).send(err.statusMessage ?? err.message ?? err.toString());
});

app.listen(5000, () => console.log("Server started on port 5000", process.env.NODE_ENV));
