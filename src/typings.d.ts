import got, { Options, OptionsOfJSONResponseBody } from "got";

export type Request = ReturnType<typeof got.stream>;
/**
 * This SHOULD be stored in a database
 */
interface IDistributor {
  name: string;
  type: "rss" | "rest";
  description?: string;
  publicURL?: string;
  // OAuth2
  scope: string[];
  registerURI: string;
  authorizationURI: string;
  publishURI: string;
  // Upload (type = "rest")
  uploadURI?: string;
  state?: string;
}

/**
 * This MUST be implemented as a class or factory function server-side
 * Constructor function SHOULD receive the model's fields as arguments
 */
interface Distributor extends IDistributor {
  /**
   * MUST send back a URL to open in a new browser tab
   * type("rest" => OAuth2 authorization page)
   * type("rss" => RSS subscription page to add a feed?
   *         OR => OAuth2 authorization page to check a feed update?)
   */
  register(): Promise<string>;
  /**
   * Asks and stores the client's credentials
   * Also handles asking and storing a long-lived access token
   */
  authorize(code: string, state: string): Promise<void>;
  /**
   * Turn any request into an authenticated request
   * - add Authorization header
   * - add query params
   * - add cookies
   * Also handles refreshing token
   */
  authenticate(url: string | URL, options?: Options): Promise<[URL, OptionsOfJSONResponseBody]>;
  /**
   * Refreshes and stores the client's credentials
   * (private method used by authorize)
   */
  refresh(token: string, state?: string): Promise<Credentials>;

  /**
   * Publishing content
   *
   * type("rest" => WritableStream = Request.body)
   * type("rss" => WritableStream = File.xml)
   */
  publish(content: ETXStudioContent): Promise<string>;

  // toBody(content: ETXStudioContent): Promise<PublisherBody>;
  // //      \/
  // //      ||
  // inject(body: PublisherBody): Promise<ReadableStream<PublisherBody>>;
  // //      \/
  // //      ||
  // send(payload: ReadableStream<PublisherBody>): Promise<WritableStream<PublisherBody>>;
}

export interface RESTDistributor extends Distributor {
  /**
   * This method MAY be called before send() if file is ArrayBuffer
   * (SHOULD NOT be called if a hosting URL is sent as a pointer to the media file)
   * @yields {String} The newly created media ID (MUST be a string)
   */
  upload?(file: ArrayBuffer, type: string): Promise<string>;
}
export interface RESTPageDistributor extends RESTDistributor {
  /**
   * A REST distributor might be able to publish on many pages
   * @yields {Array<Object>} A list of all pages from which the user MUST choose
   */
  getPagesList(userId: string): Promise<Array<{ id: string; name: string; }>>;
}
export interface RSSDistributor extends Distributor {
  /**
   * Times and seasons pass... Seconds become minutes then become hours
   * type("rss" => content publication SHOULD be checked)
   * @yields {Boolean} true if content is actually published by the RSS subscriber
   */
  check?(content: ETXStudioContent): Promise<boolean>;
}

type Credentials = {
  distributor: string;
  userId: string;
  accessToken: string;
  refreshToken?: string;
  expiresAt: number;
};
type PublisherBody = string | ArrayBuffer;
type ETXStudioContent = {
  text?: string;
  audio: string;
  video?: string;
  pageId?: string;
};
